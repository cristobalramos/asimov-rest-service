<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Appointment;
use App\User;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Creates a new appointment
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $resource = Appointment::create([
            'name' => $request->name,
            'email' => $request->email,
            'start' => $request->start,
            'end' => $request->end,
        ]);

        return response()->json($resource);
    }

    /**
     * View an appointment by id
     *
     * @param int $id
     * @return Response
     */
    public function view($id)
    {
        $appt = Appointment::where('id', $id)->first();

        return response()->json($appt);
    }

    /**
     * Update/edit an appointment by id
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $appt = Appointment::updateOrCreate(
            [
                'id' => $id
            ],
            [
                'name' => $request->name,
                'email' => $request->email,
                'start' => $request->start,
                'end' => $request->end,
            ]
        );

        return response()->json($appt);
    }

    /**
     * Returns a array with all the appointments between to dates passed by $request
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $outputData = [];
        if (!is_null($request->startDate) && !is_null($request->endDate)) {
            $resources = Appointment::whereBetween('start', [$request->startDate, $request->endDate])
                                ->get();
        } else {
            $resources = Appointment::all();
        }
        $resources->each(function ($r) use (&$outputData) {
            $outputData[] = array(
                'id' => $r->id,
                'name' => $r->name,
                'email' => $r->email,
                'start' => $r->start,
                'end' => $r->end,
            );
        });

        return response()->json($outputData);
    }
}
